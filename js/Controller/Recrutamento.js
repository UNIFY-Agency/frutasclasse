$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label-frutasclasse").addClass("selected").html(fileName);
});
